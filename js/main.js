var Main = {};

Main.onLoad = function () {
	"use strict";
	 if (window.tizen === undefined) {
        
         setTimeout(
         		function(){
         			 Main.log('This application needs to be run on Tizen device');
         		},
         		1000
         );
     }else{
     	Main.init();
        setTimeout(function() { console.log('La aplicacion ha iniciado'); }, 11000);
     }
};

Main.init = function(){
	document.getElementById('res').innerHTML = webapis.productinfo.getVersion();
	document.getElementById('res2').innerHTML = webapis.productinfo.getFirmware();
	document.getElementById('res3').innerHTML = webapis.productinfo.getDuid();
	document.getElementById('res4').innerHTML = webapis.productinfo.getModelCode();
	document.getElementById('res5').innerHTML = webapis.productinfo.getModel();
	document.getElementById('res6').innerHTML = webapis.productinfo.isTtvSupported();
	TVServerType();
}

function TVServerType(){
	switch (webapis.productinfo.getSmartTVServerType()){
		case 0:
			document.getElementById('res7').innerHTML = 'Operativo';
		break;
		case 1:
			document.getElementById('res7').innerHTML = 'Development';
		break;
		case 2:
			document.getElementById('res7').innerHTML = 'Developing';
		break;
	}
}